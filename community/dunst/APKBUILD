# Contributor: Dmitry Zakharchenko <dmitz@disroot.org>
# Contributor: Leo <thinkabit.ukim@gmail.com>
# Contributor: Stefan Wagner <stw@bit-strickerei.de>
# Maintainer: Stefan Wagner <stw@bit-strickerei.de>
pkgname=dunst
pkgver=1.7.2
pkgrel=0
pkgdesc="Customizable and lightweight notification-daemon"
url="https://dunst-project.org/"
arch="all"
license="BSD-3-Clause"
makedepends="libxscrnsaver-dev libxinerama-dev libxrandr-dev
	libnotify-dev dbus-dev wayland-dev perl pango-dev wayland-protocols"
depends="dunstify"
subpackages="$pkgname-doc dunstify"
source="$pkgname-$pkgver.tar.gz::https://github.com/dunst-project/dunst/archive/v$pkgver.tar.gz"

case "$CARCH" in
	mips64|s390x|riscv64)
		options="!check" ;; # tests depend on librsvg
	*)
		checkdepends="$checkdepends dbus librsvg bash" ;;
esac

build() {
	make all dunstify
}

check() {
	make test
}

package() {
	make DESTDIR="$pkgdir" SYSCONFDIR=/etc PREFIX=/usr install
	install -Dm755 dunstify "$pkgdir"/usr/bin/dunstify
}

dunstify() {
	pkgdesc="notify-send compatible command with additional features"
	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/dunstify "$subpkgdir"/usr/bin
}

sha512sums="
1fc5837da2ff343d9badf2af2f91e32d79b91d3376b6cbe53ed9d73a248dc6dd7752a19523743b464121af329c81e69b02b03e3c5e0daa9e20afe6eb901f65dc  dunst-1.7.2.tar.gz
"
