# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kalendar
pkgver=0.3.0
pkgrel=0
pkgdesc="A calendar application using Akonadi to sync with external services (NextCloud, GMail, ...)"
# armhf blocked by qt5-qtdeclarative
# s390x and riscv64 blocked by polkit -> akonadi
# ppc64le blocked by kaccounts-integration -> akonadi
arch="all !armhf !s390x !riscv64 !ppc64le"
url="https://invent.kde.org/pim/kalendar"
license="GPL-3.0-or-later AND BSD-2-Clause"
depends="
	kdepim-runtime
	kirigami2
	qt5-qtlocation
	"
makedepends="
	akonadi-contacts-dev
	akonadi-dev
	eventviews-dev
	extra-cmake-modules
	kcalendarcore-dev
	kconfigwidgets-dev
	kcontacts-dev
	kcoreaddons-dev
	ki18n-dev
	kirigami2-dev
	kitemmodels-dev
	kpackage-dev
	kpeople-dev
	kwindowsystem-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtlocation-dev
	qt5-qtquickcontrols2-dev
	qt5-qtsvg-dev
	"
subpackages="$pkgname-lang"
source="https://download.kde.org/stable/kalendar/kalendar-$pkgver.tar.xz"
options="!check" # No tests

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
7706823a8b04dcec0aa34616744e96724cf45bce4334fc3a2234576a0b2ff2893ade830ef66e6b5e109001db3c991b5220569737248aa78c288fdc652028c08b  kalendar-0.3.0.tar.xz
"
